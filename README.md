# Git, GitLab, RStudio HowTo

A compact intro to set-up and start using Git and GitLab in RStudio. And this in a sImple RMarkdown document, so you'll learn some RMarkdown on the go. 

RStudio is assumed to be installed. Otherwise --> www.RStudio.com

# To get started:

## 1. Create Gitlab account on https://gitlab.com/

## 2. Fork this HowTo to your own account (this means: you make an on-line copy)
    
    Log in to your Gitlab account
    Go to https://gitlab.com/tcox/git_howto
    Click fork
    Done!

## 3. Install git on your PC --> https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

**In MS Windows** Don't forget to untick the ‘Git Credential Manager’ checkbox during the Git for Windows installer
See: https://stackoverflow.com/questions/37182847/how-to-disable-git-credential-manager-for-windows

## 4. Clone this HowTo from within RStudio
  
  Get a local copy on your PC to start work on is called **cloning**
  
  In RStudio
    
    File -> New Project -> Version Control -> Git
    Repository URL: https://gitlab.com/[YourGitLabUserName]/Git_Howto
    Working directory: choose what you like (But not where you already have files you are working on)
    
**Congratulations: you can now start working on a local copy of the remote repository**
  
  
## 5. What next?
  
  You can knit the Rmd, and read the full HowTo. (If you don't know what this means --> https://rmarkdown.rstudio.com/authoring_quick_tour.html)
  
  You can make changes to the HowTo, commit them to git, and push them to your own repo. (If you don't know what this means: read the full HowTo first)
  
  If you made changes you believe should be in this HowTo, create a merge request: https://docs.gitlab.com/ee/workflow/forking_workflow.html
  
  I'll review what you suggest, and either accept or decline the merge request (haha, I'm the master)


Success!

